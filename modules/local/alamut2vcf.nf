/*
Exporting data from Alamut to textfile and generating
vcf-file for exported variants
*/
process Alamut2vcf {
    input:
        path alamutdb
        path ref
        path exclude

    output:
        path "${alamutdb.getSimpleName()}.vcf" , emit:vcf
        path "excluded_interpretations.csv"    , emit:excluded
        path "${exclude}"                      , emit:exclude_list

    script:
    // If the exclude param is not default, add the --exclude option
    def optional_exclusion = exclude.name != 'NO_FILE' ? "--exclude $exclude" : ''
    script:
    """
    alamut2vcf.py --db $alamutdb --fasta $ref --output "${alamutdb.getSimpleName()}.vcf" $optional_exclusion
    """
    stub:
    """
    touch ${alamutdb.getSimpleName()}.vcf excluded_interpretations.csv
    """

}