process Pedant {
  // Debug true prints error from Pedant
  debug true

  input:
  path inputfile
  path ref

  output:
  val true

  shell:
  """
  /petimeter validate --debug $inputfile --reference $ref

  """
  stub:
  """
  test -f $inputfile 
  test -f $ref
  """
}
