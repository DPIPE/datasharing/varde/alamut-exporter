/*
Sorting
Decomposing
Normalising
Removing INFO/gNomen and INFO/OLD_VARIANT
*/

process NormVt {
    containerOptions = "--user root"

    input:
        path vcf
        path ref

    output:
        path "${vcf.getSimpleName()}.final.vcf" , emit:vcf


    script:
    """
    vt sort ${vcf} -o "${vcf.getSimpleName()}.sorted.vcf" 

    vt decompose -s "${vcf.getSimpleName()}.sorted.vcf" -o ${vcf.getSimpleName()}.decomposed.vcf

    vt normalize \\
    -r ${ref} \\
    "${vcf.getSimpleName()}.decomposed.vcf" \\
    -o "${vcf.getSimpleName()}.normalized.vcf"

    vt rminfo "${vcf.getSimpleName()}.normalized.vcf" -t gNomen -o "${vcf.getSimpleName()}.final.vcf"

    """
    stub:
    """
    touch ${vcf.getSimpleName()}.final.vcf
    """
}
