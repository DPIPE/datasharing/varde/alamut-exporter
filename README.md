# Alamut-exporter

Code to prepare variant classification data from Alamut software for inclusion in Varde.

To run:

- Change paths in run_alamut_exporter
- Run with "bash run_alamut_exporter.bash".
- Check with pedant e.g docker run --rm -it -v $PWD:/data registry.gitlab.com/dpipe/datasharing/varde/pedant:v1.2.0 validate /data/MGENvariants.final.vcf --reference /data/reference/IGSR/genome/HumanG1Kv37/human_g1k_v37.fasta

### Exclude interpretations

The optional parameter `--exclude` expects a semicolon separated csv-file containing criteria for exclusion of interpretations.
All interpretations excluded based on this list will be written to `excluded_interpretations.csv` found in the output directory.

Allowed columns are: id, variant_id, created, updated, updated_by, acmg, classification, assembly, chromosome, start, end, inserted, deleted, type, gNomen, created_date, updated_date. Columns with other names will be ignored. The fields 'created' and 'updated' needs to be on the following format: `2022-09-20T16:50:27`, 'created_date' and 'updated_date' `2022-09-20`

If there are multiple non-empty columns in the same row, they will be combined into one expression combined by &.
Empty or missing columns will be interpreted as all values allowed for interpretations to be excluded.

Example:

Remove all interpretations located on chromosome 1 with start position 1

```
chromosome;start
1;1
```
