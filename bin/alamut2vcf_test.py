import unittest
from alamut2vcf import filter_import, handle_exclusions, parse_exclude_file, col_types_extended, col_types
import pandas as pd
from pandas.testing import assert_frame_equal
import os
import numpy as np

base_folder = os.getcwd()
test_path = os.path.join(base_folder,"test")
vcf_cols = ['CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO']

class Test_filter_import(unittest.TestCase):
	def test_is_newest_interpretation_kept(self):
		input_df = pd.read_csv(os.path.join(test_path, 'multiple_updates.csv'), sep=';')
		expected_df = pd.DataFrame({'CHROM':["X"], 'POS':[1], 'ID':['.'], 'REF':['T'], 'ALT':['A'], 'QUAL':['.'],'FILTER':['.'], 'INFO':['gNomen=ChrX(GRCh37):any_gNomen;CLASS=5;YEAR=2022']}) 
		result_df = filter_import(input_df, vcf_cols, pd.DataFrame(columns=vcf_cols))
		assert_frame_equal(result_df, expected_df)

	def test_class_i_latest_interpretation(self):
		input_df = pd.read_csv(os.path.join(test_path, 'class_0_newest_interpretation.csv'), sep=';')
		result_df = filter_import(input_df, vcf_cols, pd.DataFrame(columns=vcf_cols))
		# Expect empty df, as no interpretations are kept.
		# We have to make sure it has the same column names and dtypes as the result df:
		expected_df = pd.DataFrame(columns = result_df.columns).astype(result_df.dtypes.to_dict())
		assert_frame_equal(result_df, expected_df)

	def test_exclude_df_not_empty(self):
		input_df = pd.read_csv(os.path.join(test_path, 'multiple_updates.csv'), sep=';')
		result_df = filter_import(input_df, vcf_cols, pd.DataFrame({"chromosome":["X"], "start":[1]}))
		# Expect empty df, as no interpretations are kept.
		# We have to make sure it has the same column names and dtypes as the result df:
		expected_df = pd.DataFrame(columns = result_df.columns).astype(result_df.dtypes.to_dict())
		assert_frame_equal(result_df, expected_df)


class Test_handle_exclusions(unittest.TestCase):
	def test_exclude_user(self):
		input_df = pd.read_csv(os.path.join(test_path, 'two_users.csv'), sep=';')
		exclude_df = pd.read_csv(os.path.join(test_path, 'exclude_user.csv'), sep=';')
		expected_df = pd.DataFrame({'id':[0], 'variant_id':[1],'created':['2022-09-20T16:16:27'],'updated':['2022-09-20T16:16:27'],'updated_by':['testuser2'], 'acmg':[float('nan')],'classification':[3],'assembly':['GRCh37'],'chromosome':['X'],'start':[1],'end':[1],'inserted':['A'],'deleted':['T'], 'type':['any_type'],'gNomen':['any_gNomen']})
		result_df, exclusion_df = handle_exclusions(input_df, exclude_df)
		assert_frame_equal(result_df, expected_df)
		
	def test_exclude_position(self):
		input_df = pd.read_csv(os.path.join(test_path, 'two_positions.csv'), sep=';')
		exclude_df = pd.read_csv(os.path.join(test_path, 'exclude_position.csv'), sep=';')
		expected_df = pd.DataFrame({'id':[0], 'variant_id':[1],'created':['2022-09-20T16:16:27'],'updated':['2022-09-20T16:16:27'],'updated_by':['testuser'], 'acmg':[float('nan')],'classification':[3],'assembly':['GRCh37'],'chromosome':['X'],'start':[1],'end':[1],'inserted':['A'],'deleted':['T'], 'type':['any_type'],'gNomen':['any_gNomen']})
		result_df, exclusion_df = handle_exclusions(input_df, exclude_df)
		assert_frame_equal(result_df, expected_df)

	def test_exclude_updated(self):
		input_df = pd.read_csv(os.path.join(test_path, 'two_positions.csv'), sep=';')
		exclude_df = pd.read_csv(os.path.join(test_path, 'exclude_updated.csv'), sep=';')
		expected_df = pd.DataFrame({'id':[0], 'variant_id':[1],'created':['2022-09-20T16:16:27'],'updated':['2022-09-20T16:16:27'],'updated_by':['testuser'], 'acmg':[float('nan'),],'classification':[3],'assembly':['GRCh37'],'chromosome':['X'],'start':[1],'end':[1],'inserted':['A'],'deleted':['T'], 'type':['any_type'],'gNomen':['any_gNomen']})
		result_df, exclusion_df = handle_exclusions(input_df, exclude_df)
		assert_frame_equal(result_df, expected_df)
	
	def test_exclude_updated_date(self):
		input_df = pd.read_csv(os.path.join(test_path, 'two_positions.csv'), sep=';')
		exclude_df = pd.read_csv(os.path.join(test_path, 'exclude_updated_date.csv'), sep=';')
		expected_df = pd.DataFrame(columns= input_df.columns.values)
		expected_df = expected_df.astype(input_df.dtypes.to_dict())
		result_df, exclusion_df = handle_exclusions(input_df, exclude_df)
		assert_frame_equal(result_df, expected_df)

	def test_invalid_col_name(self):
		input_df = pd.read_csv(os.path.join(test_path, 'two_positions.csv'), sep=';')
		exclude_df = pd.read_csv(os.path.join(test_path, 'exclude_invalid_col_name.csv'), sep=';')
		expected_df = pd.DataFrame({'id':[0, 1], 'variant_id':[1,1],'created':['2022-09-20T16:16:27','2022-09-20T16:16:27'],'updated':['2022-09-20T16:16:27','2022-09-20T16:50:27'],'updated_by':['testuser', 'testuser'], 'acmg':[float('nan'), float('nan')],'classification':[3, 5],'assembly':['GRCh37','GRCh37'],'chromosome':['X','X'],'start':[1, 12],'end':[1, 12],'inserted':['A', 'A'],'deleted':['T', 'T'], 'type':['any_type', 'any_type'],'gNomen':['any_gNomen', 'any_gNomen']})
		result_df, exclusion_df = handle_exclusions(input_df, exclude_df)
		assert_frame_equal(result_df, expected_df)
	
	def test_invalid_and_valid_col_name(self):
		input_df = pd.read_csv(os.path.join(test_path, 'two_positions.csv'), sep=';')
		exclude_df = pd.read_csv(os.path.join(test_path, 'exclude_one_valid_and_invalid_col_names.csv'), sep=';')
		expected_df = pd.DataFrame({'id':[0], 'variant_id':[1],'created':['2022-09-20T16:16:27'],'updated':['2022-09-20T16:16:27'],'updated_by':['testuser'], 'acmg':[float('nan'),],'classification':[3],'assembly':['GRCh37'],'chromosome':['X'],'start':[1],'end':[1],'inserted':['A'],'deleted':['T'], 'type':['any_type'],'gNomen':['any_gNomen']})
		result_df, exclusion_df = handle_exclusions(input_df, exclude_df)
		assert_frame_equal(result_df, expected_df)

	def test_half_empty_columns_in_exclude_list(self):
		input_df = pd.read_csv(os.path.join(test_path, 'two_positions.csv'), sep=';', dtype=col_types_extended)
		exclude_df = pd.read_csv(os.path.join(test_path, 'exclude_empty_columns.csv'), sep=';', na_values='', dtype=col_types_extended)
		expected_df = pd.DataFrame({'id':[0], 'variant_id':[1],'created':['2022-09-20T16:16:27'],'updated':['2022-09-20T16:16:27'],'updated_by':['testuser'], 'acmg':[pd.NA],'classification':[3],'assembly':['GRCh37'],'chromosome':['X'],'start':[1],'end':[1],'inserted':['A'],'deleted':['T'], 'type':['any_type'],'gNomen':['any_gNomen']})
		expected_df[['id', 'variant_id','start','classification','end']] = expected_df[['id','variant_id', 'start','classification','end']].astype('Int64')
		result_df, exclusion_df = handle_exclusions(input_df, exclude_df)
		assert_frame_equal(result_df, expected_df)

	def test_full_record_in_exclude_list(self):
		input_df = pd.read_csv(os.path.join(test_path, 'two_positions.csv'), sep=';', dtype=str)
		expected_df = pd.DataFrame(columns= input_df.columns.values)
		result_df, exclusion_df = handle_exclusions(input_df, input_df)
		assert_frame_equal(result_df, expected_df)

	def test_cols_different_type(self):
		input_df = pd.read_csv(os.path.join(test_path, 'two_positions.csv'), sep=';', na_values='', dtype=col_types_extended)
		expected_df = pd.DataFrame(columns= input_df.columns.values)
		expected_df = expected_df.astype(col_types)
		exclude_df = input_df.copy()
		result_df, exclusion_df = handle_exclusions(input_df, exclude_df)
		assert_frame_equal(result_df, expected_df)


class Test_parse_exclude_file(unittest.TestCase):
	def test_dtype_chromosome_col(self):
		exclude_df = parse_exclude_file(os.path.join(test_path, 'exclude_position_chrom_number.csv'))
		assert pd.api.types.is_string_dtype(exclude_df.dtypes['chromosome'])

	def test_dtype_chromosome_not_present(self):
		parse_exclude_file(os.path.join(test_path, 'exclude_user.csv'))

	def test_empty_file(self):
		parse_exclude_file(os.path.join(test_path, 'empty.csv'))


if __name__ == '__main__':
	unittest.main()
