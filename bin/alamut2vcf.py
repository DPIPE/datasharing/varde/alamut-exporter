#!/usr/bin/env python3
import argparse
import sqlite3
import pandas as pd
import pysam

col_types = {
    "id": "Int64",  # This type is pandas int64 which can handle NaN values
    "variant_id": "Int64",
    "created": str,
    "updated": str,
    "updated_by": str,
    "acmg": str,
    "classification": "Int64",
    "assembly": str,
    "chromosome": str,
    "start": "Int64",
    "end": "Int64",
    "inserted": str,
    "deleted": str,
    "type": str,
    "gNomen": str,
}

col_types_extended = col_types.copy()

col_types_extended.update(
    {
        "created_date": str,
        "updated_date": str,
    }
)


def alamut_export(db):
    conn = sqlite3.connect(db)
    df = pd.DataFrame()
    df = pd.read_sql_query(
        "SELECT id, variant_history.variant_id, created, \
                            updated, updated_by, acmg, classification, \
                            assembly, chromosome, start, end, inserted, deleted, \
                            type, gNomen \
                            from variant_history \
                            left join variant on variant_history.variant_id=variant.variant_id",
        conn,
    )
    conn.close()
    df["inserted"] = df["inserted"].str.decode("utf-8")
    df["deleted"] = df["deleted"].str.decode("utf-8")
    return df.astype(col_types)


def handle_exclusions(df, exclude):
    # Make an empty df where we add variants we remove
    df_copy = df.copy()
    df_copy["created_date"] = pd.to_datetime(
        df_copy["created"], format="%Y-%m-%dT%H:%M:%S"
    ).dt.strftime("%Y-%m-%d")
    df_copy["updated_date"] = pd.to_datetime(
        df_copy["updated"], format="%Y-%m-%dT%H:%M:%S"
    ).dt.strftime("%Y-%m-%d")
    excluded_interpretations = pd.DataFrame(columns=df_copy.columns)
    # Check that exclude list has valid column names
    allowed_cols = df_copy.dtypes.to_dict()
    for col in exclude:
        if col not in allowed_cols:
            print(f"{col} is not a valid exclusion parameter, ignoring.")
            exclude.drop(col, axis=1, inplace=True)
        elif df_copy[col].dtype != exclude[col].dtype:
            raise TypeError(
                f"Column '{col}' is of type: {exclude[col].dtype} expected type: {df_copy[col].dtype}"
            )

    # Make a string with a logical expression that will be used for filtering based on the exclusion-df
    exclusion_strings = [
        [
            " & ".join(
                f'{k} == "{v}"' if type(v) == str else f"{k} == {v}"
                for k, v in list_.items()
                if pd.notna(v)
            )
        ][0]
        for list_ in exclude.to_dict("records")
    ]
    # Loop through the filter strings and add the variants to the excluded_interpretations df
    for filter_string in exclusion_strings:
        print(f"Excluding interpretations where {filter_string}")
        exclude = df_copy.query(filter_string)
        excluded_interpretations = pd.concat([excluded_interpretations, exclude])
    # Remove excluded interpretations
    if excluded_interpretations.empty:
        print("No interpretations excluded.")
    else:
        print(f"{len(excluded_interpretations)} interpretations excluded.")
        df = df[~df["id"].isin(excluded_interpretations["id"])].reset_index(drop=True)
    return df, excluded_interpretations


# TODO: refactor column rearrangement into separate function
def filter_import(df, vcf_cols, exclude):
    df_filtered = df.copy()
    # Make sure chromosome column is of string type
    df["chromosome"] = df["chromosome"].astype(str)
    excluded_interpretations = pd.DataFrame()
    # Filter on exclusion df
    if not exclude.empty:
        # Filter on exclusion df
        df_filtered, excluded_interpretations = handle_exclusions(df_filtered, exclude)
        # Write the interpretation excluded by the exclude-parameter to file
    excluded_interpretations.to_csv(
        "excluded_interpretations.csv", index=False, sep=";"
    )

    # Converting date columns to datetime
    df_filtered["created"] = pd.to_datetime(
        df_filtered["created"], format="%Y-%m-%dT%H:%M:%S"
    )
    df_filtered["updated"] = pd.to_datetime(
        df_filtered["updated"], format="%Y-%m-%dT%H:%M:%S"
    )
    # Removing assemblies different than GRCh37
    df_filtered = df_filtered[df_filtered["assembly"] == "GRCh37"]

    # Removing variants >=50 bp)
    df_filtered = df_filtered[abs(df_filtered["start"] - df_filtered["end"]) < 50]

    # TODO: refactor column rearrangement into separate function
    # Removing columns and renaming
    df_filtered = df_filtered.drop(
        columns=["id", "variant_id", "created", "updated_by", "acmg"]
    )
    filtered_cols = [
        "chromosome",
        "start",
        "deleted",
        "inserted",
        "assembly",
        "gNomen",
        "classification",
        "updated",
    ]
    df_filtered = df_filtered[filtered_cols]
    df_filtered = df_filtered.rename(
        columns={
            "chromosome": "CHROM",
            "start": "POS",
            "deleted": "REF",
            "inserted": "ALT",
        },
        errors="raise",
    )

    # Remove duplicates with oldest date
    df_duplicates_removed = (
        df_filtered.sort_values(by="updated")
        .groupby(["CHROM", "POS", "REF", "ALT"])
        .tail(1)
        .reset_index()
    )

    # Removing class=0
    df_duplicates_removed = df_duplicates_removed[
        ~(df_duplicates_removed["classification"] == 0)
    ]

    # TODO: refactor column rearrangement into separate function
    # Adding INFO, ID, QUAL, FILTER columns
    df_duplicates_removed["INFO"] = (
        "gNomen=Chr"
        + df_duplicates_removed["CHROM"].astype("str")
        + "("
        + df_duplicates_removed["assembly"]
        + "):"
        + df_duplicates_removed["gNomen"]
        + ";CLASS="
        + df_duplicates_removed["classification"].astype("str")
        + ";YEAR="
        + df_duplicates_removed["updated"].astype(str).str[0:4]
    )
    df_duplicates_removed["ID"] = "."
    df_duplicates_removed["QUAL"] = "."
    df_duplicates_removed["FILTER"] = "."

    # Rearranging for vcf-format
    df_duplicates_removed = df_duplicates_removed[vcf_cols]
    # dffiltered.to_csv('dffiltered.txt', sep='\t',index=False)
    return df_duplicates_removed


def ins_to_vcf(df, fasta, vcfcols):
    # Filtering variants where Alamut exports REF= and which are
    # insertions. Producing dataframe vcf.
    dfins = df[(df["REF"] == "") & (df["INFO"].str.contains("ins"))]
    dfins = dfins.reset_index(drop=True)

    # Add original position to info-field in same way as vt does when normalizing
    dfins["INFO"] = (
        dfins["INFO"]
        + ";ALAMUT_POS="
        + dfins["CHROM"].astype(str)
        + ":"
        + dfins["POS"].astype(str)
        + ":"
        + dfins["REF"]
        + "/"
        + dfins["ALT"]
    )

    # Defining start and stop to get the correct REF from pysam fasta
    dfins["start"] = dfins["POS"].sub(1)
    dfins["end"] = dfins["POS"]

    ref = pysam.FastaFile(fasta)
    for index, row in dfins.iterrows():
        dfins.loc[index, "REF_y"] = ref.fetch(row.CHROM, row.start, row.end)

    dfins_ref = dfins
    dfins_ref["REF"] = dfins_ref["REF_y"]
    dfins_ref["ALT"] = dfins_ref["REF_y"] + dfins_ref["ALT"]
    dfins_ref = dfins_ref[vcfcols]
    return dfins_ref


def dup_to_vcf(df, fasta, vcfcols):
    # Filtering variants where Alamut exports REF= and which are
    # duplications. Producing dataframe vcf.
    df_dup = df[(df["REF"] == "") & (df["INFO"].str.contains("dup"))]
    df_dup = df_dup.reset_index()

    # Add original position to info-field in same way as vt does when normalizing
    df_dup["INFO"] = (
        df_dup["INFO"]
        + ";ALAMUT_POS="
        + df_dup["CHROM"].astype(str)
        + ":"
        + df_dup["POS"].astype(str)
        + ":"
        + df_dup["REF"]
        + "/"
        + df_dup["ALT"]
    )

    # Defining start and stop to get the correct REF from pysam fasta
    df_dup["start"] = df_dup["POS"].sub(2)
    df_dup["end"] = df_dup["POS"].sub(1)
    df_dup["POS"] = df_dup["POS"].sub(1).astype(str)

    ref = pysam.FastaFile(fasta)
    for index, row in df_dup.iterrows():
        seq = ref.fetch(row.CHROM, row.start, row.end)
        df_dup.loc[index, "REF_y"] = seq

    df_dup_ref = df_dup
    df_dup_ref["REF"] = df_dup_ref["REF_y"]
    df_dup_ref["ALT"] = df_dup_ref["REF_y"] + df_dup_ref["ALT"]
    df_dup_ref = df_dup_ref[vcfcols]
    return df_dup_ref


def del_to_vcf(df, fasta, vcfcols):
    # Filtering variants where Alamut exports ALT='' and which are
    # deletions. Producing dataframe vcf.
    df_del = df[(df["ALT"] == "") & (df["INFO"].str.contains("del"))]
    df_del = df_del.reset_index(drop=True)

    # Add original position to info-field in same way as vt does when normalizing
    df_del["INFO"] = (
        df_del["INFO"]
        + ";ALAMUT_POS="
        + df_del["CHROM"].astype(str)
        + ":"
        + df_del["POS"].astype(str)
        + ":"
        + df_del["REF"]
        + "/"
        + df_del["ALT"]
    )

    # Defining start and stop to get the correct REF from pysam fasta
    df_del["start"] = df_del["POS"].sub(2)
    df_del["end"] = df_del["POS"].sub(1)
    df_del["POS"] = df_del["POS"].sub(1).astype(str)

    ref = pysam.FastaFile(fasta)
    for index, row in df_del.iterrows():
        df_del.loc[index, "REF_y"] = ref.fetch(row.CHROM, row.start, row.end)

    df_del_ref = df_del
    df_del_ref["REF"] = df_del_ref["REF_y"] + df_del_ref["REF"]
    df_del_ref["ALT"] = df_del_ref["REF_y"]
    df_del_ref = df_del_ref[vcfcols]
    return df_del_ref


def parse_exclude_file(exclude_file):
    if exclude_file == None:
        return pd.DataFrame()
    else:
        try:
            return pd.read_csv(
                exclude_file, header=0, sep=";", dtype=col_types_extended
            )
        except pd.errors.EmptyDataError:
            return pd.DataFrame()


def create_vcf(df, df_ins, df_dup, df_del, vcf_file):
    # Filtered variants without insertions and dups
    df_no_ins_dup = df[~(df["REF"] == "")]

    # Filtered variants without deletions
    df_no_ins_dup_del = df_no_ins_dup[~(df_no_ins_dup["ALT"] == "")]

    # Concatenating to get df vcf
    df_vcf = pd.concat([df_no_ins_dup_del, df_ins, df_dup, df_del])

    # Creating vcf-file with header
    header = """##fileformat=VCFv4.4
##FILTER=<ID=PASS,Description="All filters passed">
##INFO=<ID=gNomen,Number=1,Type=String,Description="gDNA-level HGVS nomenclature">
##INFO=<ID=CLASS,Number=1,Type=String,Description="Variant classification">
##INFO=<ID=YEAR,Number=1,Type=Integer,Description="Year of interpretation">
##INFO=<ID=ACMG,Number=.,Type=String,Description="ACMG criteria">
##INFO=<ID=PUBMED,Number=.,Type=String,Description="List of Pubmed IDs">
##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Structural variant length">
##INFO=<ID=ALAMUT_POS,Number=.,Type=String,Description="Original position in Alamut, chr:pos:ref:/alt encoding">
##contig=<ID=1,length=249250621>
##contig=<ID=2,length=243199373>
##contig=<ID=3,length=198022430>
##contig=<ID=4,length=191154276>
##contig=<ID=5,length=180915260>
##contig=<ID=6,length=171115067>
##contig=<ID=7,length=159138663>
##contig=<ID=8,length=146364022>
##contig=<ID=9,length=141213431>
##contig=<ID=10,length=135534747>
##contig=<ID=11,length=135006516>
##contig=<ID=12,length=133851895>
##contig=<ID=13,length=115169878>
##contig=<ID=14,length=107349540>
##contig=<ID=15,length=102531392>
##contig=<ID=16,length=90354753>
##contig=<ID=17,length=81195210>
##contig=<ID=18,length=78077248>
##contig=<ID=19,length=59128983>
##contig=<ID=20,length=63025520>
##contig=<ID=21,length=48129895>
##contig=<ID=22,length=51304566>
##contig=<ID=X,length=155270560>
##contig=<ID=Y,length=59373566>
##contig=<ID=MT,length=16569>
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO
"""

    with open(vcf_file, "w") as vcf:
        vcf.write(header)
    df_vcf.to_csv(vcf_file, sep="\t", mode="a", index=False, header=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Returns vcf with exported variants from Alamut"
    )
    parser.add_argument("--db", help="Alamut database path", required=True, type=str)
    parser.add_argument("--fasta", help="Reference fasta file", required=True, type=str)
    parser.add_argument(
        "--output", help="File name of produced vcf", required=True, type=str
    )
    parser.add_argument(
        "--exclude",
        help="File containing a table of criteria to exclude interpretations by. \
                        Format expected is csv, with semicolon separator(;). Column headers are required. Allowed columns are: \
                        id, variant_id, created, updated, updated_by, acmg, classification, assembly, chromosome, \
                        start, end, inserted, deleted, type, gNomen, updated_date, created_date. \
                        An empty field means all values of this field is accepted. \
                        If there are multiple non-empty columns in the same row, they will be combined \
                        into one expression combined by &. The excluded interpretations will be written to file.",
        required=False,
        type=str,
    )

    args = parser.parse_args()

    # vcf columns
    vcf_cols = ["CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO"]

    df = alamut_export(args.db)
    df_filtered = filter_import(df, vcf_cols, parse_exclude_file(args.exclude))
    df_vcf_ins = ins_to_vcf(df_filtered, args.fasta, vcf_cols)
    df_vcf_dup = dup_to_vcf(df_filtered, args.fasta, vcf_cols)
    df_vcf_del = del_to_vcf(df_filtered, args.fasta, vcf_cols)
    create_vcf(df_filtered, df_vcf_ins, df_vcf_dup, df_vcf_del, args.output)
