#!/bin/bash

# TODO: insert paths
FASTA_REF=/illumina/analysis/prod_pipeline/genomes/H_sapiens/b37/human_g1k_v37.fasta
OUTDIR=/illumina/analysis/dev/2023/mfahls/varde/output/
ALAMUT_DB=/illumina/analysis/dev/2023/mfahls/varde/MGENvariants.db


# TODO: Check profile
nextflow run main.nf \
-profile server_production \
-ansi \
-resume \
--fasta_ref "$FASTA_REF" \
--outdir "$OUTDIR" \
--alamut_db "$ALAMUT_DB"
