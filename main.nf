#!/usr/bin/env nextflow

/*
vim: syntax=groovy
-*- mode: groovy;-*-
*/
nextflow.enable.dsl=2

include {Alamut2vcf} from './modules/local/alamut2vcf.nf'
include {NormVt}     from './modules/local/normVt.nf'
include {Pedant}     from './modules/local/pedant.nf'

// Check input path parameters to see if they exist
def checkPathParamList = [ params.fasta_ref, params.outdir, params.alamut_db, params.exclude]
for (param in checkPathParamList) { if (param) { file(param, checkIfExists: true) } }

// Check mandatory parameters
if (params.fasta_ref) { ch_fasta = file(params.fasta_ref) } else { exit 1, 'fasta_ref not specified!' }
if (params.outdir) { ch_outdir = file(params.outdir) } else { exit 1, 'outdir not specified!' }
if (params.alamut_db) { ch_alamut_db = file(params.alamut_db) } else { exit 1, 'alamut_db not specified!' }

workflow {
    alamut_db = Channel.fromPath(file(params.alamut_db))
    ref = Channel.fromPath(file(params.fasta_ref))
    exclude = Channel.fromPath(file(params.exclude))
    Alamut2vcf(alamut_db, ref, exclude)
    NormVt(Alamut2vcf.out.vcf, ref)
    Pedant(NormVt.out.vcf, ref)
}
